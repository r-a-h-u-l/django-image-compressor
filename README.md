<!-- ABOUT THE PROJECT -->
## About The Project

A simple Image compressor which compress the original size of image and generate 10 Images of different size. User can download compressed images.

### Built With

* [Python](https://www.python.org/)
* [Django](https://www.djangoproject.com/)

### Installation

1. Download or clone the Repository to your device
2. Create Virtual Environment using `python3 -m venv <your environment name>`
3. Activate Virtual Environment for Mac and Linux user `source <virtual env path>/bin/activate`
4. Activate Virtual Environment for Windows user `venv\Scripts\activate`
5. type `pip install -r requirements.txt` (this will install required package for project)
6. type `python3 manage.py makemigrations`
7. type `python3 manage.py migrate`
8. type `python3 manage.py runserver`

### Project Snap

#### Home Page
![Screenshot_2020-12-24_at_11.29.18_AM](/uploads/1b3e6bf5eac44a9cd4176d54dd475727/Screenshot_2020-12-24_at_11.29.18_AM.png)

#### Image After Compression
![Screenshot_2020-12-24_at_11.35.56_AM](/uploads/a0ade813dfb4b77282a7de5a3c2414d6/Screenshot_2020-12-24_at_11.35.56_AM.png)
